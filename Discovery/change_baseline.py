import random
import networkx as nx

class Change:
    """This sample way (friendship paradox) is described by AAMAS 2018, End-to-End Influence Maximization in the Field
    Examples
    --------
    a = Change(fullgraph=nx.path_graph(5), budget=3)

	nx_graph = a.__call__(sparse=False)  # nx_graph is the sampling result graph 
	print(type(nx_graph))  # <class 'networkx.classes.graph.Graph'>

	sp_graph = a.__call__(sparse=True)  # sp_graph is the sparse sampling result graph 
	print(type(sp_graph))  # <class 'scipy.sparse.csr.csr_matrix'>

    Parameters
        ----------
        fullgraph : nx.Graph
            Whole adjacency matrix.
        budget : int
            Number of node queried.
    Returns
        -------
        None.
    """
    def __init__(self,fullgraph, budget=10):
        self.fullgraph = fullgraph
        self.graph = nx.Graph()
        # self.graph.add_nodes_from(self.fullgraph.nodes())
        self.budget = budget
        self.v1 = []
        self.v2 = []

    def enlarge_graph(self,u):
        assert u<len(self.fullgraph) and u>=0, "Invalid Vertex"

        self.budget -= 1
        if self.budget > 0:
            for v in self.fullgraph.neighbors(u):
                self.graph.add_edge(u,v)
    
    def sample_graph1(self):
        self.v1 = random.sample(range(len(self.fullgraph)),int(self.budget/2))
        for u in self.v1:
            self.enlarge_graph(u)
               
    def sample_graph2(self):
        for u in self.v1:
            s1 = set(self.fullgraph.neighbors(u))-(set(self.v1))
            if len(s1)>0:
                v = random.sample(s1,1)[0]
                self.enlarge_graph(v)
                self.v2.append(v)

    def sample_graph(self):
        self.sample_graph1()
        self.sample_graph2()

    def __call__(self, sparse=True):
        self.sample_graph()
        # print("Discover: ")
        # print(self.graph.nodes)
        # print("---")
        if sparse:
        	return nx.to_scipy_sparse_matrix(self.graph), list(self.graph.nodes)  # ref: https://networkx.org/documentation/stable/reference/generated/networkx.convert_matrix.to_scipy_sparse_matrix.html
        else:
        	return self.graph