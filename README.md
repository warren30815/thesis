# Graph learning security

# target attack
## 1. search space: all combinations vs direct attack

target node num: 40 nodes (ori acc top 10 and below 10 and medium 20, but only adopt those degree >= 6 and degree <= 10)<br/>
attacker: custom (ref Nettack)<br/>
attacker ability: only add 1 edge

### 1-1
observed graph: **2 hop** target node induced subgraph<br/>
**attacker search space: all combinations**<br/>
success rate: 17.5%<br/>
discovered graph node nums: 47.8<br/>
Run 1 time, Spent 1070 seconds

### 1-2
observed graph: **2 hop** target node induced subgraph<br/>
**attacker search space: direct attack (i.e. only add edge to target node)**<br/>
success rate: 15%<br/>
discovered graph node nums: 47.225<br/>
Run 1 time, Spent 38 seconds

### 1-3
observed graph: **3 hop** target node induced subgraph<br/>
**attacker search space: all combinations**<br/>
success rate: 25%<br/>
discovered graph node nums: 81<br/>
Run 1 time, Spent 3992 seconds

### 1-3
observed graph: **3 hop** target node induced subgraph<br/>
**attacker search space: direct attack (i.e. only add edge to target node)**<br/>
success rate: 20%<br/>
discovered graph node nums: 81<br/>
Run 1 time

#### 上面discovered graph node nums不一樣的原因為original model本身train會有dropout，所以每次選到的target nodes都不會完全相同
#### direct attack效率高很多

## 2. target nodes no degree limited
observed graph: 2 hop target node induced subgraph<br/>
**target node num: 40 nodes (ori acc top 10 and below 10 and medium 20)**<br/>
attacker: custom (ref Nettack)<br/>
attacker ability: only add 1 edge
success rate: 22.5%<br/>
discovered graph node nums: 36<br/>
Run 2 time avg, Spent 35 seconds

#### 以下default為40 target nodes不事先filter degree

<!-- ## 3. Larger observed graph 

### 3-1
**observed graph: 3 hop target node induced subgraph**<br/>
attacker: custom (ref Nettack)<br/>
attacker ability: only add 1 edge
success rate: 22.5~25%<br/>
discovered graph node nums: 105<br/>
Run 2 time avg, Spent 69 seconds

### 3-2
**observed graph: 4 hop target node induced subgraph**<br/>
attacker: custom (ref Nettack)<br/>
attacker ability: only add 1 edge
success rate: 25~27.5%<br/>
discovered graph node nums: 246<br/>
Run 2 time avg, Spent 130 seconds

#### 看起來direct attack的話，目前2 hop, 3hop, 4hop都差不多 -->

## 3. Larger perturbation size

**attacker ability: degree(target_node) + 2 (same as Nettack)**<br/>
attacker search space: direct attack (i.e. only add edge to target node)

**observed graph: whole graph**<br/>
attacker: custom (ref Nettack)<br/>
**success rate: 97.5%**<br/>
discovered graph node nums: 2110 (full)<br/>
Run 1 time, Spent 8520 seconds

#### 以下default為perturbation size = d + 2

## 4. restricted observed graph

attacker search space: direct attack (i.e. only add edge to target node)

#### 4-1
**observed graph: 2 hop target node induced graph**<br/>
attacker: custom (ref Nettack)<br/>
**success rate: 20%**<br/>
discovered graph node nums: 44<br/>
Run 1 time, Spent 508 seconds

#### 4-2
**observed graph: 3 hop target node induced graph**<br/>
attacker: custom (ref Nettack)<br/>
**success rate: 35%**<br/>
discovered graph node nums: 98<br/>
Run 1 time, Spent 1210 seconds

#### 4-3 (give up attacking node 862 (max one) temporarily, due to Nettack also can't attack it successfully under whole graph)
**observed graph: 4 hop target node induced graph**<br/>
attacker: custom (ref Nettack)<br/>
**success rate: 62.5%**<br/>
discovered graph node nums: 171<br/>
Run 1 time, Spent 1503 seconds (cuz omit target node 862)
