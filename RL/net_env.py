import numpy as np
import networkx as nx
from copy import copy
import matplotlib.pyplot as plt
import random
from deeprobust.graph.utils import *
from utils import get_node_embeddings

# [TODO] clip reward
class NetworkEnv(object):
    '''
    Environment for network discovery;
    Added negative penalty

    Parameters
        ----------
        observedGraph: nx.Graph
            Current known graph (init)
        fullGraph : nx.Graph
            Whole unknown graph
        fullAttributes : np.array
            all node attributes
        fullLabels : np.array
            all node labels
        attacker : class
            attacker model
        classifier : class
            classifer model
        ori_classifier_acc : float
            original (unperturbed) classifier accuracy
        device: torch.device
            torch.device("cuda:0") or torch.device("cpu")
        n_discoveries : int
            discovery budget
        n_perturbations : int
            attacker budget
        bad_reward : [TODO]
    '''

    def __init__(self, observedGraph, fullGraph, fullAttributes, fullLabels, attacker, classifier, ori_classifier_acc, device, n_discoveries=10, n_perturbations=10, bad_reward=0):
        self.init_graph = nx.Graph(observedGraph)
        self.graph = nx.Graph(observedGraph)  # current discovered graph
        self.fullGraph = nx.Graph(fullGraph)
        self.fullAttributes = fullAttributes
        self.fullLabels = fullLabels
        self.reward = 0
        self.attacker = attacker
        self.classifier = classifier
        self.ori_classifier_acc = ori_classifier_acc
        self.device = device
        self.n_discoveries = n_discoveries  # discovery budget (Act as constant)
        self.n_perturbations = n_perturbations  # attacker budget (Act as constant)
        self.bad_reward = bad_reward
        self.T = 0  # current discovery round

        modified_adj = nx.to_scipy_sparse_matrix(fullGraph)
        self.modified_adj = modified_adj.tolil()  # graph for attacker to modify and run on classifier 
        self.active = set(observedGraph.nodes)  # already discovered and selected nodes
        self.possible_actions = set()  # already discovered but not selected nodes
        for node in self.active: 
            self.enlarge_graph(node, is_init=True)  # is_init=True: won't decrease budget
        self.done = False
        self.init_state = self.get_next_state()  # dict
    
    def reset(self):     
        self.graph = nx.Graph(self.init_graph)  # current discovered graph
        self.active = set(self.init_graph.nodes)
        self.possible_actions = set()  # already discovered but not selected nodes
        for node in self.active:
            self.enlarge_graph(node, is_init=True)  
        self.reward = 0
        self.T = 0
        self.done = False

        modified_adj = nx.to_scipy_sparse_matrix(self.fullGraph)
        self.modified_adj = modified_adj.tolil()
        return self.init_state

    def enlarge_graph(self, u, is_init=False):
        '''
        Explore from given node to find neighbors
        '''
        if not is_init:
            self.T += 1

        if not self.T > self.n_discoveries:
            self.active.add(u)

            for v in self.fullGraph.neighbors(u):
                self.graph.add_edge(u,v)
                if not v in self.active:
                    self.possible_actions.add(v)
            
            if u in self.possible_actions:
                self.possible_actions.remove(u)
        # else:
        #     self.done = True
        
    def modified_graph(self, edge_perturbations_dict):
        # [TODO] maybe attacker can also modify feature?
        modified_adj = self.modified_adj
        for (v1, v2), value in edge_perturbations_dict.items():
            modified_adj[v1, v2] = value
            modified_adj[v2, v1] = value

        modified_adj = normalize_adj(modified_adj)
        modified_adj = sparse_mx_to_torch_sparse_tensor(modified_adj)
        modified_adj = modified_adj.to(self.device)
        return modified_adj

    def extract_node_attributes_array(self, graph):
        return self.fullAttributes[list(graph.nodes), :] 

    def correct_node_embedding_mapping(self, graph, e_nodes):
        mapping = {}
        nodes = graph.nodes
        for index, v in enumerate(nodes):
            mapping[v] = e_nodes[index]
        return mapping

    def get_next_state(self):
        attributes = self.extract_node_attributes_array(self.graph)
        e_nodes = get_node_embeddings(self.graph, attributes)
        next_state = self.correct_node_embedding_mapping(self.graph, e_nodes)
        next_state['csr'] = nx.to_scipy_sparse_matrix(self.graph)
        return next_state

    def step(self, action, isEND):
        '''
        Action to discover a node.
        If run out of budget or no node can explored (i.e. len(possible_actions) = 0), we stop discovery and return reward as accuracy decrease
        Else reward is 0
        
        Parameters
        ----------
            action : int
                node index
            isEND : bool
                if True, run_attacker 
        '''
        next_state = None

        if action == -1:  # no node can explored (i.e. len(possible_actions) = 0)
            print("# [TODO]")
            # [TODO]
        
        # [TODO] step reward 
        assert action in self.possible_actions
        before_enlarge_size = len(self.graph)
        self.enlarge_graph(action)
        after_enlarge_size = len(self.graph)
        self.reward = (after_enlarge_size - before_enlarge_size) / len(self.fullGraph)
        next_state = self.get_next_state()

        # [TODO] Don't understand
        # if action in self.active:
        #     self.reward = self.bad_reward
        #     self.T += 1
        #     if self.T > self.n_discoveries:
        #         self.done = True
        #     self.reward_ = parallel_influence(self.graph, full_graph=self.fullGraph,
        #                         influence=self.influence_algo, times=self.times_mean)
        #     if self.normalize:
        #         self.reward = self.reward_/self.opt_reward
        #     else:
        #         self.reward = self.reward_ - self.opt_reward
        #     if self.clip_max is not None or self.clip_min is not None:
        #         self.reward = np.clip(self.reward, self.clip_min, self.clip_max)
        #     return self.graph, self.reward, self.done, None

        if isEND:
            self.run_attacker()

        return next_state, self.reward, self.done, self.graph

    # [HOTFIX] will modify discovered graph, just for test
    def run_attacker(self):
        # print("Run Attacker")
        # Region: Attacker work
        attacker = self.attacker
        observed_adj = nx.to_scipy_sparse_matrix(self.graph)
        labels = self.fullLabels
        n_perturbations = self.n_perturbations
        perturbations_dict = attacker.attack(observed_adj, labels, n_perturbations)
        assert len(perturbations_dict) <= n_perturbations * 2  # *2 because undirected
        modified_adj = self.modified_graph(perturbations_dict)
        # Endregion

        # Region: Classifier work
        classifier = self.classifier
        perturbed_acc = classifier.__call__(modified_adj)
        # Endregion

        ans = self.ori_classifier_acc - perturbed_acc  # [TODO] rescale?
        print("Final discovered graph size: %d" % (len(self.graph)))
        print("Acc drop: %f" % (ans))
        # if self.clip_max is not None or self.clip_min is not None:
        #     self.reward = np.clip(self.reward, self.clip_min, self.clip_max)

    # @property
    # def state(self):
    #     return nx.to_numpy_matrix(self.graph)
    
    # def draw_curr(self, pos=None):
    #     '''
    #     Draw discovered graph
    #     '''
    #     plt.figure(1,figsize=(30,30))
    #     plt.clf()
    #     if pos is None:
    #         nx.draw_random(self.graph, with_labels = True)
    #     else:
    #         nx.draw(self.graph, with_labels = True, pos=pos)
    
    # def sample_action(self):
    #     if len(self.possible_actions)>0:
    #         return random.sample(self.possible_actions,1)[0]
    #     else:
    #         return -1