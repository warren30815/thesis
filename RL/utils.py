from karateclub.node_embedding.attributed import FSCNMF
import networkx as nx

def relabel_node_id(graph):
    return nx.convert_node_labels_to_integers(graph)

def get_node_embeddings(G, X):
    '''
    Parameters
    ----------
        G : nx.Graph
        X : np.array
            node attributes
    '''
    model = FSCNMF()
    relabeled_G = relabel_node_id(G)
    model.fit(relabeled_G, X)
    return model.get_embedding()  # [NOTE] will relabel automatically

