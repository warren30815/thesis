import torch
import torch.nn as nn
import torch.nn.functional as F
import random
import numpy as np
from buffer import Buffer
from datetime import datetime
import dgl
from dgl.nn.pytorch.glob import SortPooling

BATCH_SIZE = 32  
LR = 0.01                   # learning rate
EPSILON = 0.9               # greedy policy
GAMMA = 0.9                 # reward discount
TARGET_REPLACE_ITER = 75   # target update frequency
MEMORY_CAPACITY = 2000
ACTION_DIM = 64 # action embedding dimension (determined by node embedding method)
STATES_DIM = 64  # state embedding dimension  (determined by node embedding method)
POOLING = 'SortPool'

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(STATES_DIM + ACTION_DIM, 64)
        self.fc1.weight.data.normal_(0, 0.1)   # initialization
        self.out = nn.Linear(64, 1)
        self.out.weight.data.normal_(0, 0.1)   # initialization

    def forward(self, state, action):
        """
        :param state: Input state (Torch Variable : [n,state_dim] )
        :param action: Input Action (Torch Variable : [n,action_dim] )
        :return: Value function : Q(S,a) (Torch Variable : [n,1] )
        """
        x = torch.cat((state,action),dim=1)
        x = self.fc1(x)
        x = F.relu(x)
        q_value = self.out(x)
        return q_value

class DQN(object):
    def __init__(self):
        self.eval_net, self.target_net = Net(), Net()

        self.learn_step_counter = 0                                     # for target updating
        self.memory = Buffer(max_size=MEMORY_CAPACITY)
        self.optimizer = torch.optim.Adam(self.eval_net.parameters(), lr=LR)
        self.loss_func = nn.MSELoss()

    # [TODO] not sure q_eval correctness
    def choose_action(self, state, current_possible_nodes, isStartTraining):
        '''
        Parameters
        ----------
            state: dict 
                key: node id, value: node embedding
            current_possible_nodes : list
                list of node id
            isStartTraining : bool
                True if DQN starts updating
        '''
        random.seed(datetime.now())
        q_value = -10000.0
        action = -1

        if len(current_possible_nodes) != 0:
            if random.uniform(0, 1) < EPSILON:   # greedy
                if isStartTraining:
                    tmp_state = torch.FloatTensor(self.graph_pooling_from_dict([state], pooling=POOLING))
                    for v in current_possible_nodes:
                        tmp_action = torch.FloatTensor([state[v]])
                        q_eval = self.eval_net(tmp_state, tmp_action).detach()
                        if q_eval > q_value:
                            q_value = q_eval
                            action = v
                else:
                    action = random.sample(current_possible_nodes, 1)[0]
            else:   # random
                action = random.sample(current_possible_nodes, 1)[0]

        return action

    def store_transition(self, s, a, r, next_s):
        transition = np.hstack((s, [a, r], next_s))  # for example, s = next_s = {1: [1,2,3], 2: [4,5,6], ...other node & its embedding... ,'csr': scipy.csr_matrix }, a = 1, r = 5
                                                     # np.hstack((s, [a, r], next_s)) will be 
                                                     # array([{1: [1,2,3], 2: [4,5,6]...}, 1, 5, {1: [1,2,3], 2: [4,5,6]...}], dtype=object)
        # deque will replace the old memory with new memory automatically
        self.memory.add(transition)

    def learn(self):
        # target parameter update
        if self.learn_step_counter % TARGET_REPLACE_ITER == 0:
            self.target_net.load_state_dict(self.eval_net.state_dict())
        self.learn_step_counter += 1

        # sample batch transitions 
        # [TODO] prioritized replay
        b_memory = self.memory.sample(BATCH_SIZE)  # batch of transition np.array (see function store_transition)
        b_s = torch.FloatTensor(self.graph_pooling_from_dict(b_memory[:, 0], pooling=POOLING)) 
        # b_a = torch.LongTensor(b_memory[:, 1].astype(int))
        b_a = torch.FloatTensor(self.get_action_embedding(b_memory[:, 0], b_memory[:, 1]))
        b_r = torch.FloatTensor(b_memory[:, 2].astype(float))
        b_s_ = torch.FloatTensor(self.graph_pooling_from_dict(b_memory[:, 3], pooling=POOLING))

        # q_eval w.r.t the action in experience
        q_eval = self.eval_net(b_s, b_a)  # shape (batch, 1)
        q_next = self.target_net(b_s_, b_a).detach()   # detach from graph, don't backpropagate
        q_target = b_r.view(BATCH_SIZE, 1) + GAMMA * q_next   # shape (batch, 1)
        loss = self.loss_func(q_eval, q_target)

        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

    # [TODO] maybe add some graph statistics firstly (e.g. avg. degree...)
    # [TODO] DiffPool
    # [QUESTION] SumPool will make the training process unstable?
    def graph_pooling_from_dict(self, batch_dict, pooling):
        states = np.zeros((len(batch_dict), STATES_DIM))
        for index, each_dict in enumerate(batch_dict):
            if pooling == "SumPool":
                for value in each_dict.values():
                    states[index] += value
            elif pooling == "SortPool":
                sparse_graph = each_dict['csr']
                node_embeds = torch.FloatTensor()
                for key, value in each_dict.items():  # For SortPool, node embedding order won't change the result 
                    if key != 'csr':
                        value = torch.FloatTensor([value])
                        node_embeds = torch.cat((node_embeds,value),0)

                sortpool = SortPooling(k=1)
                dgl_g = dgl.from_scipy(sparse_graph)
                states[index] = sortpool(dgl_g, node_embeds)[0]
                
        return states

    def get_action_embedding(self, batch_dict, batch_action):
        actions = np.zeros((len(batch_dict), ACTION_DIM))
        for index, each_dict in enumerate(batch_dict):
            node = batch_action[index]
            actions[index] += each_dict[node]
        return actions
