#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
sys.path.append("..")
import numpy as np
import networkx as nx
import random
import torch
from dqn import DQN
from net_env import NetworkEnv
from deeprobust.graph.data import Dataset
from Classifier.gcn_classifier import GCN_classifier
from DeepRobust.deeprobust.graph.global_attack import DICE
from deeprobust.graph.utils import preprocess

# Hyper Parameters
DATASET = "citeseer"
EPOCHS = 450
WHEN_START_LEARN_DQN = 50  # get some exp in memory firstly 
MAX_DISCOVERIES = 10
MAX_PERTURBATIONS = 30
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# [Region] Load data
data = Dataset(root='/tmp/', name=DATASET)
adj, features, fullLabels = data.adj, data.features, data.labels
# print(type(adj))  # <class 'scipy.sparse.csr.csr_matrix'>
# print(type(features)) # <class 'scipy.sparse.csr.csr_matrix'>
# print(type(fullLabels)) # <class 'numpy.ndarray'>

idx_train, idx_val, idx_test = data.idx_train, data.idx_val, data.idx_test  # type: <class 'numpy.ndarray'>
fullGraph = nx.from_scipy_sparse_matrix(adj)
fullAttributes = np.array(features.toarray())
# [EndRegion]

# [Region] Setup init graph
random_starting_node = random.choice(list(fullGraph.nodes))
observedGraph = nx.ego_graph(fullGraph, random_starting_node, radius=2, center=True, undirected=True)
# [EndRegion]

# [Region] Setup Attack Model
attacker = DICE()
# [EndRegion]

# [Region] Setup Classifier Model and calculate ori_acc
classifier = GCN_classifier(data)
torch_adj, _, _ = preprocess(adj, features, fullLabels, preprocess_adj=False, sparse=True, device=device) 
ori_classifier_acc = classifier.__call__(torch_adj)
# [EndRegion]

dqn = DQN()
env = NetworkEnv(observedGraph, fullGraph, fullAttributes, fullLabels, attacker, classifier, ori_classifier_acc, device, MAX_DISCOVERIES, MAX_PERTURBATIONS)


# In[2]:


import time

print("[INFO] init graph nodes num: %s, nodes list: %s" % (str(len(observedGraph)), str(observedGraph.nodes)))
print('\nCollecting experience...')
isStartTraining = False
for i_episode in range(EPOCHS):
    start = time.time()
    print('Epoch: ', i_episode)
    state = env.reset()  # dict
    current_possible_nodes = env.possible_actions
    ep_r = 0
    isEND = False
    # while True:
    for step in range(1, MAX_DISCOVERIES+1):
        if step == MAX_DISCOVERIES:
            isEND = True
            
#         print("Go step: ", step)
        # print("[DEBUG] current_possible_nodes:")
        # print(current_possible_nodes)
        action = dqn.choose_action(state, current_possible_nodes, isStartTraining)  # action: int (node index)
        # print(action)
        # take action
        next_state, reward, _, cur_graph = env.step(action, isEND) 
#         print("[DEBUG] graph nodes: ", str(cur_graph.nodes))
        dqn.store_transition(state, action, reward, next_state)  

        ep_r += reward
        if i_episode > WHEN_START_LEARN_DQN:
            isStartTraining = True
            dqn.learn()
            # if done:
#             print('| Ep_r: ', round(ep_r, 2))

        # if done:
        #     break
        state = next_state
        current_possible_nodes = env.possible_actions
    print("Spent %f seconds" % (time.time() - start))


# In[ ]:




