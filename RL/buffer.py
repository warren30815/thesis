import numpy as np
from collections import deque
import random
from datetime import datetime

class Buffer:

    def __init__(self, max_size=1000):
        self.buffer = deque(maxlen=max_size)
        self.max_size = max_size
    
    @property
    def size(self):
        return len(self.buffer)
    
    def sample(self, BATCH_SIZE):
        random.seed(datetime.now())
        b_memory = random.sample(self.buffer, BATCH_SIZE)
        return np.array(b_memory)
    
    def add(self, transition):
        '''
        Parameters:
            transition: np.array
                Please see function store_transition() in dqn.py 
        '''
        self.buffer.append(transition)