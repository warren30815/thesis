import torch
import numpy as np
import torch.nn.functional as F
import torch.optim as optim
from deeprobust.graph.utils import *
from deeprobust.graph.defense import GCN

class GCN_classifier:
    """
    Parameters
        ----------
        dataset : deeprobust.graph.data.Dataset
            Please see https://github.com/DSE-MSU/DeepRobust/blob/master/deeprobust/graph/data/dataset.py
    Returns
        -------
        None.
    """
    def __init__(self, dataset):
        self.dataset = dataset
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.get_data()

    def get_data(self):
        data = self.dataset
        adj, features, labels = data.adj, data.features, data.labels
        _, self.features, self.labels = preprocess(adj, features, labels, preprocess_adj=False, sparse=True, device=self.device)
        # print(type(adj))  # <class 'scipy.sparse.csr.csr_matrix'>
        # print(type(features)) # <class 'scipy.sparse.csr.csr_matrix'>
        # print(type(labels)) # <class 'numpy.ndarray'>
        self.idx_train, self.idx_val, self.idx_test = data.idx_train, data.idx_val, data.idx_test  # type: <class 'numpy.ndarray'>

    def __call__(self, adj):
        features, labels = self.features, self.labels
        idx_train, idx_val, idx_test = self.idx_train, self.idx_val, self.idx_test

        gcn = GCN(nfeat=features.shape[1],
              nhid=16,
              nclass=labels.max().item() + 1,
              dropout=0.5, device=self.device)

        gcn = gcn.to(self.device)

        optimizer = optim.Adam(gcn.parameters(),
                           lr=0.01, weight_decay=5e-4)

        gcn.fit(features, adj, labels, idx_train) # train without model picking
        # gcn.fit(features, adj, labels, idx_train, idx_val) # train with validation model picking
        output = gcn.output
        # loss_test = F.nll_loss(output[idx_test], labels[idx_test])
        # acc_test = accuracy(output[idx_test], labels[idx_test])
        loss_test = F.nll_loss(output, labels)
        acc_test = accuracy(output, labels)
        # print("Test set results:",
        #       "loss= {:.4f}".format(loss_test.item()),
        #       "accuracy= {:.4f}".format(acc_test.item()))
        return round(acc_test.item(), 4)