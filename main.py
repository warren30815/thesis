import torch
import numpy as np
import torch.nn.functional as F
import torch.optim as optim
import networkx as nx
from Discovery.change_baseline import Change
from Classifier.gcn_classifier import GCN_classifier
from DeepRobust.deeprobust.graph.global_attack import DICE, PGDAttack
# from deeprobust.graph.defense import GCN
from deeprobust.graph.utils import *
from deeprobust.graph.data import Dataset
from deeprobust.graph.defense import GCN
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--seed', type=int, default=15, help='Random seed.')
parser.add_argument('--dataset', type=str, default='citeseer', choices=['cora', 'cora_ml', 'citeseer', 'polblogs', 'pubmed'], help='dataset')
parser.add_argument('--ptb_rate', type=float, default=0.05,  help='pertubation rate')

args = parser.parse_args()
args.cuda = torch.cuda.is_available()
print('cuda: %s' % args.cuda)
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

def generate_perturbation(discovery_part, n_discoveries, attack_part):
	np.random.seed(args.seed)
	torch.manual_seed(args.seed)
	if args.cuda:
	    torch.cuda.manual_seed(args.seed)

	data = Dataset(root='/tmp/', name=args.dataset)
	adj, features, labels = data.adj, data.features, data.labels
	ori_adj = adj
	# print(type(adj))  # <class 'scipy.sparse.csr.csr_matrix'>
	# print(type(features)) # <class 'scipy.sparse.csr.csr_matrix'>
	# print(type(labels)) # <class 'numpy.ndarray'>

	# idx_train, idx_val, idx_test = data.idx_train, data.idx_val, data.idx_test  # type: <class 'numpy.ndarray'>
	# idx_unlabeled = np.union1d(idx_val, idx_test)

	n_perturbations = int(args.ptb_rate * (adj.sum()//2))
	n_perturbations = 30  # [TODO]

	# Setup Discovery Model
	if discovery_part == "Whole":
		observed_adj = adj
	elif discovery_part == "Change":
		discover_agent = Change(fullgraph=nx.from_scipy_sparse_matrix(adj), budget=n_discoveries)
		observed_adj, nodes_mapping_list = discover_agent.__call__(sparse=True)  # sp_graph is the sparse sampling result graph 

	whole_graph_len = len(adj.todense())
	observed_graph_len = len(nx.from_scipy_sparse_matrix(observed_adj).nodes)

	# Setup Attack Model
	if attack_part == "DICE":
		model = DICE()
		perturbations_dict = model.attack(observed_adj, labels, n_perturbations)
		assert len(perturbations_dict) <= n_perturbations * 2  # *2 because undirected
	elif attack_part == "PGDAttack":
		import scipy
		# Setup Victim Model
		batch_features = features.toarray()
		batch_features = batch_features[nodes_mapping_list]
		batch_features = scipy.sparse.csr_matrix(batch_features)
		batch_labels = labels[nodes_mapping_list]
		idx_train = [i for i in range(len(nodes_mapping_list))]

		observed_adj, batch_features, batch_labels = preprocess(observed_adj, batch_features, batch_labels, preprocess_adj=False)  # convert to torch Tensor
		victim_model = GCN(nfeat=batch_features.shape[1], nclass=batch_labels.max().item()+1, nhid=16,
		        dropout=0.5, weight_decay=5e-4, device=device)
		victim_model = victim_model.to(device)
		victim_model.fit(batch_features, observed_adj, batch_labels, idx_train) 

		model = PGDAttack(model=victim_model, nnodes=observed_adj.shape[0], loss_type='CE', device=device)
		model = model.to(device)
		model.attack(batch_features, observed_adj, batch_labels, idx_train, n_perturbations, epochs=200)   # [TODO]

		x = observed_adj.cpu().numpy()
		y = model.modified_adj.cpu().numpy()

		diff = y - x
		sparse_diff = scipy.sparse.csr_matrix(diff)
		d = sparse_diff.todok() # convert to dictionary of keys format
		perturbations_dict = dict(d.items())
		# print(perturbations_dict)
		# print("---")
		for key, value in perturbations_dict.items():
		    if value == -1:
		    	perturbations_dict[key] = 0

	# print(perturbations_dict)
	modified_adj = adj.tolil()
	for (v1, v2), value in perturbations_dict.items():
		v1 = nodes_mapping_list[v1]
		v2 = nodes_mapping_list[v2]
		modified_adj[v1, v2] = value
	modified_adj = normalize_adj(modified_adj)
	modified_adj = sparse_mx_to_torch_sparse_tensor(modified_adj)
	modified_adj = modified_adj.to(device)

	# adj, features, labels = preprocess(adj, features, labels, preprocess_adj=False, sparse=True, device=device)
	adj, _, _ = preprocess(adj, features, labels, preprocess_adj=False, sparse=True, device=device)  # convert to torch tensor
	# print(whole_graph_len, observed_graph_len)
	return data, adj, modified_adj, whole_graph_len, observed_graph_len

# def test_GCN(adj):
#     ''' test on GCN '''
#     # adj = normalize_adj_tensor(adj)
#     gcn = GCN(nfeat=features.shape[1],
#               nhid=16,
#               nclass=labels.max().item() + 1,
#               dropout=0.5, device=device)

#     gcn = gcn.to(device)

#     optimizer = optim.Adam(gcn.parameters(),
#                            lr=0.01, weight_decay=5e-4)

#     gcn.fit(features, adj, labels, idx_train) # train without model picking
#     # gcn.fit(features, adj, labels, idx_train, idx_val) # train with validation model picking
#     output = gcn.output
#     loss_test = F.nll_loss(output[idx_test], labels[idx_test])
#     acc_test = accuracy(output[idx_test], labels[idx_test])
#     print("Test set results:",
#           "loss= {:.4f}".format(loss_test.item()),
#           "accuracy= {:.4f}".format(acc_test.item()))

#     return acc_test.item()

def main():
	import time
	start = time.time()
	repeat_time = 1
	n_discoveries = 10
	ori_acc_list = []
	perturbed_acc_list = []
	ori_graph_size_list = []
	discovered_graph_size_list = []
	discovery_part = "Change"
	attack_part = "PGDAttack"
	# attack_part = "DICE"

	for _ in range(repeat_time):
	    data, adj, modified_adj, whole_graph_len, observed_graph_len = generate_perturbation(discovery_part, n_discoveries, attack_part)
	    c = GCN_classifier(data)
	    ori_acc = c.__call__(adj)
	    perturbed_acc = c.__call__(modified_adj)
	    ori_acc_list.append(ori_acc)
	    perturbed_acc_list.append(perturbed_acc)
	    ori_graph_size_list.append(whole_graph_len)
	    discovered_graph_size_list.append(observed_graph_len)

	print("Run %d time avg result: " % (repeat_time))
	print('=== testing GCN on original(clean) graph ===')
	print(sum(ori_acc_list) / len(ori_acc_list))
	print('=== testing GCN on perturbed graph ===')
	print(sum(perturbed_acc_list) / len(perturbed_acc_list))
	print('=== original graph nodes num ===')
	print(sum(ori_graph_size_list) / len(ori_graph_size_list))
	print('=== discovered graph nodes num ===')
	print(sum(discovered_graph_size_list) / len(discovered_graph_size_list))
	print("Spent %f seconds" % (time.time() - start))

if __name__ == '__main__':
    main()

