with open('20201227_just_step_reward_and_state_is_sortpool.txt') as f:
    text = f.read()
    txt_list = text.split("\n")
    txt_list = [t for t in txt_list if t.startswith("Final discovered graph")]

before_training_avg_50epochs_node_nums = 0
after_training_avg_50epochs_node_nums = 0
batch = 50
for index, t_list in enumerate(txt_list):
    if index < batch:
        before_training_avg_50epochs_node_nums += int(t_list.split(": ")[1])
    elif index > len(txt_list) - batch:
        after_training_avg_50epochs_node_nums += int(t_list.split(": ")[1])

print(before_training_avg_50epochs_node_nums / batch)
print(after_training_avg_50epochs_node_nums / batch)
